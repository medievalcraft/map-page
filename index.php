<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Willkommen auf der Landkarte von MedievalCraft!">
    <meta name="keywords" content="Minecraft, Medieval, Roleplay">
    <meta name="author" content="Loapu, JaanHD">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/normalize.css">
    
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="js/jquery-1.9.1.min.js"></script>
    
    <title>MedievalCraft Landkarte</title>
</head>
<body>
    <div id="loader-wrapper">
    <div id="loader"></div>
 
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
 
    </div>
    
    <div id="content">
        <iframe seamless scrolling="auto" frameborder="0" src="http://176.9.25.245:8123/?worldname=MedievalCraft&mapname=flat&zoom=2&x=-5558&y=64&z=-771.9999999999997#"></iframe>
        <p style="position: absolute; z-index: 1; left: 75px; top: 0px">
        <a href="https://minecraft-server.eu/vote/index/121145" onclick="window.open(this.href); return false;"><img width="100" src="http://www.medievalcraft.de/wiki/images/3/3f/Vote_MCserver_eu_1.gif"></a>
        </p>
    </div>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="js/main.js"></script>
</body>
</html>

